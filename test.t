#include <adv.t>
#include <std.t>

startroom: room
  sdesc = "field"
  ldesc = "I was standing on top of a small hill almost precisely in the middle of a large, lusciously colored field.  The deep green of the grass offset the cloud-pockmarked sky nicely.  The field was completely surrounded by trees so darkly colored I found myself wondering if they really were the same trees I had grown up around.  There were rocks a bit to the south, and off in the distance to the west were the ever present snow-capped mountains.\nThere was a tall pillar next to me."
  south = field2;
;

hill: decoration
  location = startroom
  noun = 'hill'
  sdesc = "hill"
  ldesc = "It was the only hill in the entire field, and the pillar was at its highest point."
;

sky: decoration
  location = startroom
  noun = 'sky'
  sdesc = "sky"
  ldesc = "It was one of those days where the clouds were so perfectly positioned as to be painted on.  The inviting blue of the sky was not unusal here."
;

sky2: decoration
  location = field2
  noun = 'sky'
  sdesc = "sky"
  ldesc = "It was one of those days where the clouds were so perfectly positioned as to be painted on.  The inviting blue of the sky was not unusal here."
;

clouds: decoration
  location = startroom
  noun = 'clouds'
  sdesc = "clouds"
  ldesc = "There was no wind, and the clouds did not move."
;

clouds2: decoration
  location = field2
  noun = 'clouds'
  sdesc = "clouds"
  ldesc = "There was no wind, and the clouds did not move."
;

trees: decoration
  location = startroom
  noun = 'trees'
  sdesc = "trees"
  ldesc = "The trees were too far away for me to examine closely."
;

trees2: decoration
  location = field2
  noun = 'trees'
  sdesc = "trees"
  ldesc = "The trees were too far away for me to examine closely."
;

mow: deepverb
  verb = 'mow'
  sdesc = "mow"
  doAction = 'Cut'
;

cut: deepverb
  verb = 'cut'
  sdesc = "cut"
  doAction = 'Cut'
;

grass: decoration
  location = startroom
  noun = 'grass'
  sdesc = "grass"
  ldesc = "The grass was slightly uncomfortable against my bare feet, but its cool temperature was rather refreshing."

  verDoCut(actor) = {
    say('There was no reason to cut the grass here, and the field was large enough that it would have taken several days.');
  }

  doCut(actor) = { return(nil); }
;

mountains: distantItem
  location = startroom
  noun = 'mountains'
  adjective = 'snow-capped'
  sdesc = "mountains"
  ldesc = "The ever-present mountains had become a landmark for me to keep my bearings over the years.  I had never visited them as that would have been far too arduous a journey, but I had heard tales of journeys there for trading purposes.  Beyond them, rumor stated, was a great city-state, large enough to rival the forest itself."
;

mountains2: distantItem
  location = field2
  noun = 'mountains'
  adjective = 'snow-capped'
  sdesc = "mountains"
  ldesc = "The ever-present mountains had become a landmark for me to keep my bearings over the years.  I had never visited them as that would have been far too arduous a journey, but I had heard tales of journeys there for trading purposes.  Beyond them, rumor stated, was a great city-state, large enough to rival the forest itself."
;

pillar: decoration
  location = startroom
  noun = 'pillar' 'marble'
  adjective = 'white'
  sdesc = "pillar"
  ldesc = "The pillar was definitely made of marble, the pearly white threaded with veins of black rock.  It appeared to be  relatively new; no indication of wear was visible.  Its mirror-smooth surface reflected the field around me.\nThe pillar was decorated at both ends with intricately carved swirls, reminiscent of some ancient and incredibly powerful culture.  This was not made by any technology I knew of, but it had been there long enough to be taken for granted."

  verDoMove(actor) = { say('No one had ever tried to move the pillar, as it had never been deemed necessary.  I felt no different.'); }

  verDoTake(actor) = { say('It was far too heavy.'); }
;

swirls: decoration
  location = pillar
  noun = 'swirls' 'carvings' 'decorations'
  adjective = 'intricate'
  sdesc = "carvings"
  ldesc = "The carvings were far too detailed to be language.  They were obviously meant simply to increase the aesthetic nature of the pillar.  They were not created with a chisel, as I was familiar with that kind of work."
;

field2: room
  sdesc = "field"
  ldesc = "The rocks were rather close to the south, and the pillar to the north.  I brushed off one of my feet on the off chance that there was an insect there.  There wasn't."
  north = startroom
;
